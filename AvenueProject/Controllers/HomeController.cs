﻿using System.Web.Mvc;

namespace AvenueProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Welcome()
        {
            return View("~/Views/Home/Welcome.cshtml");
        }
    }
}