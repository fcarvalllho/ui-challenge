﻿app.controller('WelcomeCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.lat = 43.6222102;
    $scope.long = -79.6694881;
    $scope.myLatLng = {};
    $scope.mapUrl = "";
    $scope.map = {};
    $scope.marker = {};

    $scope.initMap = function () {
        ShowOverlay();
        $scope.myLatLng = {
            lat: $scope.lat,
            lng: $scope.long
        };

        $scope.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: $scope.myLatLng
        });

        $scope.marker = new google.maps.Marker({
            position: $scope.myLatLng,
            map: $scope.map,
        });
        HideOverlay();
    }

    $scope.resetMap = function () {
        ShowOverlay();
        var getUrl = $scope.getValidUrl($scope.mapUrl);
        if (getUrl == false) {
            alert("Invalid Url, please follow the example: http://www.mysite.com");
            HideOverlay();
        } else {
            $http.get('http://freegeoip.net/json/' + getUrl).then(function (response) {
                //var panTo = new google.maps.LatLng(response.data.latitude, response.data.longitude);
                $scope.lat = response.data.latitude;
                $scope.long = response.data.longitude;
                $scope.initMap();
            }).catch(function(e) {
                alert("The current address is not a valid one." + " Erro: " + e.data);
            }).finally(function () {
                HideOverlay();
            });
        }
    }

    $scope.getValidUrl = function (url) {

        if (url.indexOf(' ') >= 0) {
            alert("Url error: Remove all white spaces.");
        } else {
            getTextAndLengh = url.split("http://");
            var tempUrl = getTextAndLengh[1];
            var getLength = tempUrl.split('.');
            if (getLength.length > 3) {
                url = false;
            } else {
                url = tempUrl;
            }
        }
        return url;
    }

}]);