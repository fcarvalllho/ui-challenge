﻿var app = angular.module('app', [
    'ngRoute',
    'ui.grid',
    'ui.grid.pagination'
]);

app.config(function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode(false);

    //Rota utilizando ng-route
    //os parametros informados são a URL do controller do c# que retorna uma partialView (TemplateUrl)
    //E a controller do AngularJS que gerenciará essa partialView
    //Os links e chamadas são feitos na página Home/Index

    debugger
    $routeProvider
        .when("/About", {
            templateUrl: "Home/About"
        })
        .when("/Welcome", {
            templateUrl: "Home/Welcome",
            controller: "WelcomeCtrl"
        })

        .otherwise({ redirectTo: '/Welcome'})
});