# README #

About the used technologies and how to build it.

### What is this repository for? ###

* Quick summary
* 1.0.0

### How do I get set up? ###

This website was developed in Microsoft C# .Net MVC framework, I used the visual studio suit to create an empty MVC project and then I set my basic configurations to work properly.

This site is a SPA (Single page application) created with ui-router and AngularJS in the front-end. To create the admin layout I use AdminLTE CSS, an open source library to help with clean responsive HTML template.

I'd not used TDD because I'm not familiarezed with the technology, until now and my last three year of experience all of my projects was build without thinking of TDD. But a long of those years I was always using design patterns, in this project you will see the MVC pattern, but I'm familiarezed with MVVM, DDD, MVW, and other conceptual patterns.

So if you want download this project to start with .Net MVC C#, first thing you need to do is download the visual studio community from microsoft website. Then you just need to clone this repository to your local manchine and set the Avenue Project as startup project.

The Main flux in this project start at the Angular app.js in script folder. In the app.js you will see the ui-router configurantion and the module configuration. After you insert a route in ui-router you will need point this route to the c# controller in the controllers folder. You can return as View(), partialView() or url, any of those options will work, by the end all those need to be a HTML template.

You can set to an AngularJS controller in ui-router like the others that was already there. The AngularJS controller is created in the ViewScripts folder, once they ask directly for an page(View). After create the scripts and point your ui-router you just need set the Angularjs controller refence in the _layout page to replicate in the project. After this all you need to do is put your ui-router state in the menu on Index.cshtml on the Home folder in Views.

Now your project is read to build it. :)